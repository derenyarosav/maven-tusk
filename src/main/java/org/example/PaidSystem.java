package org.example;

import java.util.Scanner;

public class PaidSystem {
    static final int PRICE_POR_KILOMETER = 35;

    public void counter(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("We are glad to welcome you in the taxi!");
        System.out.println("Enter the distance to the destination : ");
        double distance = scanner.nextDouble();
        System.out.println("The  price is : " + distance * PRICE_POR_KILOMETER + " UAH");
    }
}
